import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
 
  defaultText: string = 'Looks like you have nothing scheduled for today!';
  tasklist: string[] = [];
  displayForm: boolean = false;

  onCreateTask(formdata: any){
    this.tasklist.push(formdata.createTask);
    formdata.createTask = '';
    console.log(this.tasklist);
    this.displayForm = false;
  }

  onCreateRequest(){
    this.displayForm = true;
  }
}
